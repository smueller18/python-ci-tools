#!/usr/bin/env python

"""Generator of HTML documents for linting results."""

import os
import json
from itertools import groupby
from operator import itemgetter
from collections import OrderedDict
from jinja2 import FileSystemLoader, Environment


CI_PROJECT_URL = os.getenv("CI_PROJECT_URL", "")
CI_COMMIT_REF_NAME = os.getenv("CI_COMMIT_REF_NAME", "master")
TARGET = os.getenv("LINT_TARGET", "target/lint")

lint_data = json.load(open(TARGET + "/pylint.json"))

_ROW_CLASSES = {
    "refactor": "table-primary",
    "convention": "table-primary",
    "warning": "table-warning",
    "error": "table-error",
    "fatal": "table-error",
}

_COLUMN_NAMES = OrderedDict({
    "line": "Line",
    "column": "Column",
    "type": "Type",
    "obj": "Object",
    "message": "Message",
    "symbol": "Symbol",
    "message_id": "Message Id",
})

modules = sorted(lint_data, key=itemgetter('module'))

data = OrderedDict()

for module, rows in groupby(modules, key=itemgetter('module')):
    data.update({module: list()})
    rows = sorted(rows, key=itemgetter('line'))
    for row in rows:
        data[module].append({
            "class": _ROW_CLASSES[row["type"]],
            "url": CI_PROJECT_URL + "/blob/" + CI_COMMIT_REF_NAME + "/" + row["path"] + "#L" + str(row["line"]),
            "path": row["path"],
            "row": OrderedDict({
                "line": row["line"],
                "column": row["column"],
                "type": row["type"],
                "obj": row["obj"],
                "message": row["message"].replace("\n", "<br />"),
                "symbol": row["symbol"],
                "message_id": row["message-id"],
            })
        })


template = Environment(loader=FileSystemLoader(searchpath=os.path.dirname(
    os.path.abspath(__file__)))).get_template("index.html.j2")
with open(TARGET + "/index.html", "w+") as f:
    f.write(template.render(data=data, column_names=_COLUMN_NAMES))
